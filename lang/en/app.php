<?php 

return array(
		'HOME' => 'HOME',
		'COMPANY' => 'COMPANY PROFILE',
		'INDONESIAN_SPICES' => 'INDONESIAN SPICES & COMMODITIES',
		'MARKETS' => 'MARKETS',
		'CONTACT_US' => 'CONTACT US',

		// contact form
		'Name'=>'Name',
		'Company'=>'Company',
		'Email'=>'Email',
		'Phone'=>'Phone',
		'Address'=>'Indonesian Spices & Commodities Interest',
		'Message'=>'Message',

		'enquire_product'=>'ENQUIRE PRODUCT',

		// Homepage
		'fcs_text_content' => '<h2>Running the Indonesian Spices & Commodities business since 1979, PT. Tiga Rasa Indonesia is one of the pioneers specialized in Spices, Herbs and Dried Commodities.</h2>
							<p>In this business, experience counts! Yes it is! PT. Tiga Rasa has brought and presented Indonesian spices to the international market. We introduce to the world, the highest quality products which sourced from all corners of this fertile and rich nation of Indonesia.</p>',
		'fcs_link_text' => 'LEARN MORE ABOUT OUR COMPANY',


		'market_desc_block2_title' =>'',
		'market_desc_block2_desc' =>'<p>PT. Tigarasa Indonesia has been operating for more than 20 years, that`s one of the reason the distribution of our products throughout the big cities and even villages in Indonesia has been evenly distributed.</p>
							<p>To maintain the market share stability, the only key to our success is quality products. Another thing supporting our existence is the punctuality, accuracy and assurance of supply with a consistency that we always shown from time to time.</p>',
	);


?>