<?php 

return array(
		'HOME' => 'BERANDA',
		'COMPANY' => 'PROFIL PERUSAHAAN',
		'INDONESIAN_SPICES' => 'REMPAH &amp; KOMODITI INDONESIA',
		'MARKETS' => 'PASAR',
		'CONTACT_US' => 'HUBUNGI KAMI',
		
		// contact form
		'Name'=>'Nama',
		'Company'=>'Perusahaan',
		'Email'=>'Email',
		'Phone'=>'Telepon',
		'Address'=>'Indonesian Spices & Commodities Interest',
		'Message'=>'Pesan',
		
		'enquire_product'=>'INKUIRI PRODUK',

		// Homepage
		'fcs_text_content' => '<h2>Menjalankan bisnis Rempah & Komoditas Indonesia sejak tahun 1979, PT. Tiga Rasa Indonesia adalah salah satu pelopor yang mengkhususkan pada rempah-rempah, herbal dan komoditas kering.</h2>
							<p>Dalam bisnis ini, pengalaman penting! Ya! PT. Tiga Rasa telah membawa dan menyajikan rempah-rempah Indonesia ke pasar internasional. Kami mengenalkan kepada dunia, produk dengan kualitas terbaik yang bersumber dari segala penjuru negeri subur dan kaya ini.</p>',
		'fcs_link_text' => 'pelajari lebih lanjut tentang perusahaan kami',


		'market_desc_block2_title' =>'',
		'market_desc_block2_desc' =>'<p>PT. Tiga Rasa Indonesia telah beroperasi lebih dari 20 tahun sehingga penyebaran distribusi produk kami di seluruh kota besar maupun pelosok di Indonesia telah merata.</p>
							<p>Untuk menjaga kestabilan market share, satu satunya kunci keberhasilan kami adalah kualitas. Hal lain yang mendukung adalah ketepatan serta jaminan pasokan yang dengan stabil dan konsisten kami lakukan dari waktu ke waktu.</p>',
	);


?>